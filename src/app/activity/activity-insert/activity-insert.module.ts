import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ActivityInsertRoutingModule } from './activity-insert-routing.module';
import { AddActivityComponent } from './components/add-activity/add-activity.component';
import { MaterialModule } from 'src/app/material/material.module';


@NgModule({
  declarations: [
    AddActivityComponent
  ],
  imports: [
    CommonModule,
    ActivityInsertRoutingModule,
    MaterialModule
  ]
})
export class ActivityInsertModule { }
