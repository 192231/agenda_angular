import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/core/services/user.service';
import { FormGroup } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-add-activity',
  templateUrl: './add-activity.component.html',
  styleUrls: ['./add-activity.component.css']
})
export class AddActivityComponent implements OnInit {
  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
  ) {
    this.form = this.formBuilder.group({
      idUsuario: ['', [Validators.required]],
      Titulo: ['', [Validators.required]],
      FechaProgramada: ['', [Validators.required]],
      Hour: ['', null],
    });
  }
  ngOnInit(): void {}
  buildForm() {

  }
  user() {
    if (this.form.valid) {
      const formData = new FormData();
      formData.append('dto.idUsuario',this.form.controls['idUsuario'].value);
      formData.append('dto.titulo',this.form.controls['Titulo'].value);
      formData.append('dto.fechaProgramada',this.form.controls['FechaProgramada'].value);
      formData.append('dto.horaProgramada', this.form.controls['Hour'].value);
      this.userService.User(formData).subscribe((rpt) => {
        console.log(rpt);
      });
    }
  }

}
