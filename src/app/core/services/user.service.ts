import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {}
    User(formData:FormData){
      return this.http.post('https://localhost:7036/Usuario/Insert',formData);
    }

}
